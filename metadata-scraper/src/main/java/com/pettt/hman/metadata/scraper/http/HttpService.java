package com.pettt.hman.metadata.scraper.http;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class HttpService {

    public String getRequest(String url, ProxyReservation.Proxy proxy) throws IOException {
        return executeRequest(RequestType.GET, url, null, responseHandler(), proxy, null);
    }

    public String executeJsonPost(String apiUrl, String requestBody, ProxyReservation.Proxy proxy) throws IOException {
        return executeRequest(RequestType.POST_JSON, apiUrl, requestBody, responseHandler(), proxy, null);
    }

    public String executeJsonForm(String apiUrl, List<NameValuePair> payload, ProxyReservation.Proxy proxy) throws IOException {
        return executeRequest(RequestType.POST_FORM, apiUrl, payload, responseHandler(), proxy, null);
    }

    @SuppressWarnings("SameParameterValue") //TODO implement cookie support (i.e. exhentai)
    private String executeRequest(RequestType requestType, String url, Object payload,
                                  ResponseHandler<String> responseHandler,
                                  ProxyReservation.Proxy proxy, Set<Cookie> cookies)
            throws IOException {
        log.trace("Request http get: {}", url);
        String hostname = proxy.getHostname();
        String port = String.valueOf(proxy.getPort());

        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(70000)
                .setSocketTimeout(70000).build();

        HttpHost host = new HttpHost(hostname,Integer.valueOf(port), "http");
        DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(host);
        CookieStore store = new BasicCookieStore();
        HttpClientBuilder builder = HttpClientBuilder.create()
                .setDefaultCookieStore(store)
                .setRoutePlanner(routePlanner)
                .setDefaultRequestConfig(requestConfig)
                .setConnectionTimeToLive(70, TimeUnit.SECONDS)
                ;

        CloseableHttpClient httpclient = builder.build();
        store.clear();
        if(cookies != null) cookies.forEach(store::addCookie);

        switch (requestType) {
            case GET:{
                HttpGet httpget = new HttpGet(url);
                return httpclient.execute(httpget, responseHandler);
            }
            case POST_JSON:{
                HttpPost httpPost = new HttpPost(url);

                if(!(payload instanceof String)) {
                    log.error("POST payload is not a json string!");
                    throw new RuntimeException("Wrong format for JSON payload");
                }

                StringEntity postingString = new StringEntity((String) payload);
                httpPost.setEntity(postingString);
                httpPost.setHeader("Content-type", "application/json");

                return httpclient.execute(httpPost, responseHandler);
            }
            case POST_FORM:{
                HttpPost httpPost = new HttpPost(url);

                //noinspection unchecked
                httpPost.setEntity(new UrlEncodedFormEntity((List<NameValuePair>) payload));
                httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");

                return httpclient.execute(httpPost, responseHandler);
            }
            default:{
                throw new IllegalStateException("Request type not handled: "+requestType);
            }
        }
    }



    public enum RequestType{
        GET, POST_JSON, IMAGE, POST_FORM
    }

    private ResponseHandler<String> responseHandler() {
        return response -> {
            int status = response.getStatusLine().getStatusCode();
            log.trace("Response code: '{}'", status);
            if (status >= 200 && status < 300) {
                HttpEntity entity = response.getEntity();
                return entity != null ? EntityUtils.toString(entity) : null;
            } else if(status == 403) {
                return "403: banned proxy";
            } else {
                throw new IOException("Unexpected response status: " + status);
            }
        };
    }
}
