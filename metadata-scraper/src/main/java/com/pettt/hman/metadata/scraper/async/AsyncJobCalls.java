package com.pettt.hman.metadata.scraper.async;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pettt.hman.metadata.scraper.config.Executors;
import com.pettt.hman.metadata.scraper.http.ProxyReservation;
import com.pettt.hman.metadata.scraper.http.HttpService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.List;
import java.util.function.Consumer;

import static com.pettt.hman.metadata.scraper.http.HttpService.*;

@SuppressWarnings({"SpringAutowiredFieldsWarningInspection", "unchecked", "Duplicates"})
@Service @Slf4j
public class AsyncJobCalls {
    @Autowired private HttpService httpService;
    @Autowired private ProxyReservation proxyReservation;
    @Autowired private Executors executors;

    private ObjectMapper mapper = new ObjectMapper();

    @SneakyThrows
    private void scrapeAndForward(RequestType type, String url, Object payload,
                                  String savePath, String proxyGroup, int holdOnProxy,
                                  Consumer<String> forward, String outputFormat, String message) throws IOException {
        log.trace("Scraping {}", url);

        // reserve proxy for group
        ProxyReservation.Proxy proxy = proxyReservation.reserveProxyBlock(proxyGroup);

        //get valid response
        String response = null;
        while(response == null){
            try { // try http execution
                response = executeResponse(type, proxy, url, payload);

                validateResponse(response, proxy);
            } catch (IOException e) { //failed: mark proxy appropriately and try again
                log.debug("Failed request: {}   Mark proxy {} as BAD", e.getMessage(), proxy);
                proxyReservation.retryProxy(proxy,proxyGroup);

                //reserve a new proxy
                proxy = proxyReservation.reserveProxyBlock(proxyGroup);
                response = null;
            } catch (ProxyBannedException | ProxyFaultyException e) { //TODO check faulty responses
                proxyReservation.banProxy(proxy, proxyGroup);
                log.warn("{}; {} banned proxies in group '{}'.",
                        e.getMessage(), proxyReservation.getBannedProxies(proxyGroup), proxyGroup);

                //reserve a new proxy
                proxy = proxyReservation.reserveProxyBlock(proxyGroup);
                response = null;
            }
        }

        //write output
        saveOutput(savePath,response,outputFormat);
        log.info("Finished scraping {}  {}", url, (message == null ? "" : message));

        //forward if needed
        if(forward != null) forward.accept(new File(savePath).getPath());

        //hold on for delay
        Thread.sleep(holdOnProxy);

        //finish request
        proxyReservation.releaseProxy(proxy, proxyGroup);
    }

    @SneakyThrows
    private void imageAndForward(String url, String savePath, String proxyGroup, int holdOnProxy,
                                  Consumer<String> forward) throws IOException {
        log.trace("Scraping {}", url);

        // reserve proxy for group
        ProxyReservation.Proxy proxy = proxyReservation.reserveProxyBlock(proxyGroup);

        //get valid response
        BufferedImage response = null;
        while(response == null){
            try { // try http execution
                response = ImageIO.read(new URL(url));
            } catch (IOException e) { //failed: mark proxy appropriately and try again
                log.debug("Failed request: {}   Mark proxy {} as BAD", e.getMessage(), proxy);
                proxyReservation.retryProxy(proxy,proxyGroup);

                //reserve a new proxy
                proxy = proxyReservation.reserveProxyBlock(proxyGroup);
                response = null;
            }
        }

        //write output
        File saveOutput = new File(savePath);
        if(!saveOutput.getParentFile().exists() && !saveOutput.getParentFile().isDirectory()) {
            boolean mkdirs = saveOutput.getParentFile().mkdirs();
            if(!mkdirs) throw new IOException("Could not create folders at " + savePath);
        }
        saveOutput.delete();
        ImageIO.write(response, "jpg", saveOutput);
        log.info("Finished fetching image {}", url);

        //forward if needed
        if(forward != null) forward.accept(new File(savePath).getPath());

        //hold on for delay
        Thread.sleep(holdOnProxy);

        //finish request
        proxyReservation.releaseProxy(proxy, proxyGroup);
    }

    private void validateResponse(String response, ProxyReservation.Proxy proxy) throws ProxyBannedException, ProxyFaultyException {
        if(response.isEmpty())
            throw new ProxyBannedException("Response is empty!");

        if(response.contains("This content is not available in Russia")) //exhentai response
            throw new ProxyBannedException("Proxy is banned in Russia! Proxy: "+proxy.getHostname());

        if(response.contains("Your IP address has been temporarily banned")) //exhentai response
            throw new ProxyFaultyException("Proxy is banned (temporary IP ban)! Proxy: "+proxy.getHostname());

        if(response.equals("403: banned proxy"))
            throw new ProxyBannedException("Proxy is banned (403)! Proxy: "+proxy.getHostname());

        if(response.contains("صفحه مورد نظر در دسترس نمی باشد")) //nhentai response
            throw new ProxyBannedException("Bad arabic response! Proxy: "+proxy.getHostname());

        if(response.contains("пїЅпїЅпїЅпїЅпїЅпїЅ")) //nhentai response
            throw new ProxyBannedException("Bad russian response! Proxy: "+proxy.getHostname());
    }

    private String executeResponse(RequestType type, ProxyReservation.Proxy proxy, String url, Object payload) throws IOException {
        switch (type) {
            case GET:
                return httpService.getRequest(url,proxy);
            case POST_JSON:
                return httpService.executeJsonPost(url, (String) payload, proxy);
            case POST_FORM:
                return httpService.executeJsonForm(url, (List<NameValuePair>) payload, proxy);
            default:
                throw new RuntimeException("Response type not handled! "+type);
        }
    }

    //saves the content into the given file at path
    private void saveOutput(String savePath, String content, String outputFormat) throws IOException {
        File saveOutput = new File(savePath);
        if(!saveOutput.getParentFile().exists() && !saveOutput.getParentFile().isDirectory()) {
            boolean mkdirs = saveOutput.getParentFile().mkdirs();
            if(!mkdirs) throw new IOException("Could not create folders at " + savePath);
        }

        saveOutput.delete();

        if(outputFormat.equalsIgnoreCase("json")){
            Object json = mapper.readValue(content, Object.class);
            content = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
        }

        try( PrintWriter out = new PrintWriter(saveOutput) ) {
            out.println( content );
        }
    }

    // ---------------
    // PUBLIC API
    // ---------------

    //wrapper for async calls
    public void scrapeHtmlAndForward(String scrapeUrl, String savePath, String proxies, int release, Consumer<String> forwardedStart, String type) {
        Runnable r = () -> {
            try {
                scrapeAndForward(RequestType.GET, scrapeUrl, null, savePath, proxies, release, forwardedStart, type, null);
            } catch (IOException e) {
                log.error("Failed GET request: {}", e.getMessage());
            }
        };

        executors.execute(proxies, r);
    }

    //non-forward version
    public void scrapeHtml(String scrapeUrl, String savePath, String proxies, int release, String type) {
        scrapeHtmlAndForward(scrapeUrl, savePath, proxies, release, null, type);
    }

    public void scrapeApiAndForward(String apiUrl, String requestBody, String savePath,
                                    String proxies, int release, Consumer<String> forwardedStart,
                                    String type, String message) {
        Runnable r = () -> {
            try {
                scrapeAndForward(RequestType.POST_JSON, apiUrl, requestBody, savePath,
                        proxies, release, forwardedStart, type, message);
            } catch (IOException e) {
                log.error("Failed JSON POST request: {}", e.getMessage());
            }
        };

        executors.execute(proxies, r);
    }

    //non-forward version
    public void scrapeApi(String apiUrl, String requestBody, String savePath, String proxies, int release, String type, String message) {
        scrapeApiAndForward(apiUrl, requestBody, savePath, proxies, release, null, type, message);
    }

    public void scrapeApiFormAndForward(String apiUrl, List<NameValuePair> pairList, String savePath,
                                        String proxies, int release, Consumer<String> forwardedStart, String type, String message) {
        Runnable r = () -> {
            try {
                scrapeAndForward(RequestType.POST_FORM, apiUrl, pairList, savePath, proxies, release, forwardedStart, type, message);
            } catch (IOException e) {
                log.error("Failed JSON POST request: {}", e.getMessage());
            }
        };

        executors.execute(proxies, r);
    }

    public void scrapeApiForm(String apiUrl, List<NameValuePair> pairList, String savePath, String proxies, int release, String type, String message) {
        scrapeApiFormAndForward(apiUrl, pairList, savePath, proxies, release, null, type, message);
    }

    public void scrapeImage(String scrapeUrl, String savePath, String proxies, int release, String type) {
        scrapeImageAndForward(scrapeUrl, savePath, proxies, release, null, type);
    }

    public void scrapeImageAndForward(String scrapeUrl, String savePath, String proxies, int release, Consumer<String> forwardedStart, String type) {
        Runnable r = () -> {
            try {
                imageAndForward(scrapeUrl,  savePath, proxies, release, forwardedStart);
            } catch (IOException e) {
                log.error("Failed GET request: {}", e.getMessage());
            }
        };

        executors.execute(proxies, r);
    }

    static class ProxyBannedException extends Exception{
        ProxyBannedException(String message) {
            super(message);
        }
    }
    static class ProxyFaultyException extends Exception{
        ProxyFaultyException(String message) {
            super(message);
        }
    }
}