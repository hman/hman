package org.pivk.util;

import org.pivk.util.impl.ImagePHash;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

@SuppressWarnings("unused") //utility
public final class ImageUtil {
    private ImageUtil(){ throw new UnsupportedOperationException("Utility class instantiation."); }

    /**
     * Calculates a string hash representation of the given image in an input stream as consecutive 0 or 1 bits.
     *
     * @param inputStream image provided as an {@link InputStream}
     * @return hash representation of image ( "0101010111..,")
     * @throws IOException if image can not be found via ImageIO.read(inputStream)
     * @since 0.1
     */
    public static String calculateHash(InputStream inputStream) throws IOException {
        return ImagePHash.getInstance().getHash().getHash(inputStream);
    }

    /**
     * Calculates a string hash representation of the given image as consecutive 0 or 1 bits.
     *
     * @param bufferedImage image provided as a {@link BufferedImage}
     * @return String hash representation of image ( "0101010111..,")
     * @since 0.1
     */
    public static String calculateHash(BufferedImage bufferedImage)  {
        return ImagePHash.getInstance().getHash().getHash(bufferedImage);
    }
}
