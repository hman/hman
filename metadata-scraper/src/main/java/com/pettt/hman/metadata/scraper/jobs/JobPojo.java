package com.pettt.hman.metadata.scraper.jobs;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Map;

@Data
public class JobPojo {
    public int id;
    public String name;
    public String proxies;

    public int depends;
    public int forward;
    public int release;
    public boolean force;
    public Map<String, Object> input;
    public Map<String, Object> output;

    @JsonIgnore
    Object getOutput(String key) {
        return output.get(key);
    }

    @JsonIgnore
    Object getInput(String key) {
        return input.get(key);
    }
}
