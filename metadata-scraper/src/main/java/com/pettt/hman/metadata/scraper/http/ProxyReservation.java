package com.pettt.hman.metadata.scraper.http;

import com.pettt.hman.metadata.scraper.util.StringUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
public class ProxyReservation {
    private final Map<String, Set<Proxy>> freeProxies = new ConcurrentHashMap<>(1000);
    private final Map<String, Set<Proxy>> warningProxies = new ConcurrentHashMap<>(1000);
    private final Map<String, Set<Proxy>> errorProxies = new ConcurrentHashMap<>(1000);

    private final Set<Proxy> knownProxies = new HashSet<>();

    public void insertProxies(String proxyPath, Set<String> proxyGroups) throws IOException {
        log.info("Reading proxy list at {}", proxyPath);
        String body = StringUtil.readBody(new File(proxyPath));

        for (String proxy : body.split("\n")) {
            String hostname = proxy.split(":")[0];
            String port = proxy.split(":")[1];
            knownProxies.add(new Proxy(hostname,Integer.parseInt(port)));
        }

        //create synchonized sets
        for (String proxyGroup : proxyGroups) {
            freeProxies.put(proxyGroup, Collections.synchronizedSet(new HashSet<>()));
            warningProxies.put(proxyGroup, Collections.synchronizedSet(new HashSet<>()));
            errorProxies.put(proxyGroup, Collections.synchronizedSet(new HashSet<>()));

            synchronized (freeProxies.get(proxyGroup)) {
                freeProxies.get(proxyGroup).addAll(knownProxies);
            }
        }

        log.info("{} known unique proxies, {} proxy groups", knownProxies.size(), proxyGroups.size());
    }

    private Proxy reserveProxy(String proxyGroup) {
        Proxy result;
        synchronized (freeProxies.get(proxyGroup)){
            Iterator<Proxy> it = freeProxies.get(proxyGroup).iterator();
            if(it.hasNext()){
                result = it.next();
                it.remove();
            }else{
                result = null;
            }
        }

        return result;
    }

    @SneakyThrows //ignore sleep exception
    public Proxy reserveProxyBlock(String proxies) {
        Proxy proxy = this.reserveProxy(proxies);
        while(proxy == null) {
            Integer minutes = StringUtil.getRandomMinute(30);
            log.info("No free proxies available. Retry again in {} minutes.",minutes);
            Thread.sleep(minutes * 60000);
            proxy = reserveProxy(proxies);
        }
        return proxy;
    }

    public int getBannedProxies(String proxies) {
        synchronized (errorProxies.get(proxies)) {
            return errorProxies.get(proxies).size();
        }
    }

    public void banProxy(Proxy proxy, String proxyGroup) {
        synchronized (errorProxies.get(proxyGroup)) {
            errorProxies.get(proxyGroup).add(proxy);
        }
    }

    public void retryProxy(Proxy proxy, String proxyGroup) {
        synchronized (warningProxies.get(proxyGroup)) {
            warningProxies.get(proxyGroup).add(proxy);
        }
    }

    public void releaseProxy(Proxy proxy, String proxyGroup) {
        synchronized (freeProxies.get(proxyGroup)) {
            freeProxies.get(proxyGroup).add(proxy);
        }
    }

    public void resetBadProxies(Set<String> proxyGroups) {
        Map<String, List<Proxy>> proxyGroupToAdd = new HashMap<>();
        for (String proxyGroup : proxyGroups) {
            proxyGroupToAdd.put(proxyGroup, new ArrayList<>());
        }

        for (String proxyGroup : proxyGroups) {
            synchronized (warningProxies.get(proxyGroup)) {
                Iterator<Proxy> it = warningProxies.get(proxyGroup).iterator();
                while(it.hasNext()){
                    proxyGroupToAdd.get(proxyGroup).add(it.next());
                    it.remove();
                }
            }

            synchronized (freeProxies.get(proxyGroup)) {
                freeProxies.get(proxyGroup).addAll(proxyGroupToAdd.get(proxyGroup));
                log.info("Retry {} bad proxies for {} group", proxyGroupToAdd.get(proxyGroup).size(), proxyGroup);
            }
        }
    }


    @Data @AllArgsConstructor
    public static class Proxy{
        String hostname;
        int port;
    }
}
