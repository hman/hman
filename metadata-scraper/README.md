# Metadata scraper v0.5.x

Job-based Metadata scraper for paginated websites or websites which provide an api. 
Job output files can be the input of other jobs to scrape a hierarchical structure.

Example jobs provided for
* `nhentai.net`
    * [Gallery scrape job](jobs/nhen/nhenGalleryHtml.scrape): Scrapes all nhentai galleries as HTML files.
    Forwards each HTML file to the nhentai metadata job.
    * [Metadata scrape job](jobs/nhen/nhenMetadataHtml.scrape): Takes the input of the gallery scrape job 
    and parses it for gallery ids. Uses parsed ids to fetch gallery metadata as HTML files. Forwards
    each html file to the nhentai json extractor job
    * [Json extractor job](jobs/nhen/nhenMetadataConvert.scrape): Takes the input of the metadata scrape job 
    and extracts the json file out of it, creating a small json file containing the metadata of the gallery.
* `e-hentai.net` and `api.e-hentai.net/api.php`
    * [Gallery scrape job](jobs/ehen/ehenGalleryHtml.scrape): Scrapes all e-hentai galleries as HTML files.
    Forwards each HTML file to the e-hentai api metadata job.
    * [Metadata scrape job](jobs/ehen/nhenMetadataHtml.scrape): Takes the input of the gallery scrape job 
    and parses it for gallery ids and tokens. Uses parsed ids and the 
    [POST body request template](jobs/ehen/ehenBody.api) to generate a POST request for the e-hentai api.
    * [Thumbnail scrape job](jobs/ehen/ehenThumbnails.scrape): Uses the thumbnail url in the JSON response 
    to fetch the thumbnail.
* `tsumino.com`
    * [Gallery scrape job](jobs/tsumino/tsuminoMetadataJson.scrape): Scrapes all tsumino galleries as JSON files.
    Uses the provided [form](jobs/tsumino/tsuminoBody.form) for API POST requests.
    
Use these templates to write your own scrape jobs for other sites until I document the .scrape format better.

## .scrape Format Explained
    
`TODO`

# Usage

* Build project with `gradle build` (or the gradlew wrapper) or 
download a pre-built executable jar `metadata-scraper-v0.4.x.jar`
* Execute the jar with following parameters:
    * `proxies:[path]`
        * `[path]`: Path to a `.csv` file containg **https** proxies. One proxy per line. Format: `hostname:port`
    * `scrape:[path]`
        * `[path]`: Path to a single `.scrape` file
    * `scrapeFolder:[path]`
        * `[path]`: Path to a folder containing `.scrape` files
        
* The scraper can be restarted anytime, downloaded files will not be downloaded again
* Banned proxies are detected and not used again
* Other failures disable the proxy, after 3000 seconds all bad proxies which are not banned are resetted.
* *Warnings* and *errors* are logged to a file in the execution folder.

# Example Usage

The jar can be started multiple times for each group or only once with all job files provided.
If you start multiple jar executions, spread the command by minutes, thus creating one log file per
jar execution.

## First jar execution: ehentai jobs

![](.wiki/v0.2.0example0.jpg)

## Second jar execution: nhentai jobs

![](.wiki/v0.2.0example1.jpg)


## Changelog

### v0.2

* Rewritten engine to allow users to define their own jobs

### v0.3

* Extractor job type

### v0.4

* Api POST form job type

### v0.5

* Image fetcher